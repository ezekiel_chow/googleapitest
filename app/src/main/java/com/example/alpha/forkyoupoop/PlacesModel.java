package com.example.alpha.forkyoupoop;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by leikeze on 7/14/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlacesModel {
    String placeLat;
    String placeLng;
    String placeID;
    String placeName;
    String userID;
    String pushID;

    public PlacesModel(){
        // empty default constructor, necessary for Firebase
    }

    public String getPlaceLat()
    {
        return placeLat;
    }

    public String getPlaceLng()
    {
        return placeLng;
    }

    public String getPlaceID()
    {
        return placeID;
    }

    public String getPlaceName()
    {
        return placeName;
    }

    public String getUserID()
    {
        return userID;
    }

    public String getPushID()
    {
        return pushID;
    }
}
