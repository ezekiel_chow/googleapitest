package com.example.android.multidex.forkyoupoop;

import android.Manifest;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.alpha.forkyoupoop.PlacesModel;
import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.varunest.sparkbutton.SparkButton;
import com.varunest.sparkbutton.SparkEventListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class PopularPlaces extends AppCompatActivity implements OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowCloseListener {

    private GoogleMap mMap;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    /**
     * Request code for location permission request.
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    /**
     * Flag indicating whether a requested permission has been denied after returning in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     */
    private boolean mPermissionDenied = false;

    private String placeIDr;
    private String placeNamer;
    private double placeLat;
    private double placeLng;
    private Location whereAmI;
    private List<PlacesModel> foodPlaces;
    private SparkButton favouriteButton;
    private Map<String, String> markersAndPlaceID;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popular_places);

        foodPlaces = new ArrayList<PlacesModel>();

        favouriteButton = (SparkButton)findViewById(R.id.favouriteBtn);
        favouriteButton.setChecked(false);

        placeIDr = getIntent().getExtras().getString("PlaceID");
        placeNamer = getIntent().getExtras().getString("PlaceName");
        placeLat = getIntent().getExtras().getDouble("PlaceLat");
        placeLng = getIntent().getExtras().getDouble("PlaceLng");

        markersAndPlaceID = new HashMap<String, String>();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        GoogleMapOptions mapOptions = new GoogleMapOptions();

        mapOptions.zoomControlsEnabled(true)
                .zoomGesturesEnabled(true)
                .zoomControlsEnabled(true)
                .compassEnabled(true)
                .rotateGesturesEnabled(true);

        mapFragment.newInstance(mapOptions);

        mapFragment.getMapAsync(this);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this)
                .addApi(AppIndex.API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        //Firebase
        Firebase.setAndroidContext(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowCloseListener(this);
        giveMyLocation();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void giveMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            com.example.android.multidex.forkyoupoop.PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (com.example.android.multidex.forkyoupoop.PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            giveMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        com.example.android.multidex.forkyoupoop.PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    protected void onStart() {
        client.connect();
        super.onStart();
    }

    protected void onStop() {
        client.disconnect();
        super.onStop();
    }

    private void addPolylines(LatLng start, LatLng destination)
    {
        //Constructing link
        final PolylineOptions lineOptions = new PolylineOptions()
                .color(Color.BLUE)
                .width(5);

        String baseURL = "https://maps.googleapis.com/maps/api/directions/json?";
        String origin = "origin=" + start.latitude + "," + start.longitude;
        String destinationString = "destination=" + destination.latitude + "," + destination.longitude;
        String serverKey = "key=" + "AIzaSyAuv0OXbFeQaly74HjYymnzZguvofv_FzY";

        String fullURL = baseURL + origin + "&" + destinationString + "&" + serverKey;
        Log.d("fullURL", fullURL);
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, fullURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Directions", response);
                try
                {
                    JSONObject mainObj = new JSONObject(response);
                    JSONArray routesArray = mainObj.getJSONArray("routes");
                    JSONObject routesObj = routesArray.getJSONObject(0);
                    JSONArray legsArray = routesObj.getJSONArray("legs");
                    JSONObject legsObj = legsArray.getJSONObject(0);
                    JSONArray stepsArray = legsObj.getJSONArray("steps");

                    for (int i = 0; i < stepsArray.length(); i++) {
                        JSONObject eachPoint = stepsArray.getJSONObject(i);
                        JSONObject startObj = eachPoint.getJSONObject("start_location");
                        JSONObject endObj = eachPoint.getJSONObject("end_location");

                        lineOptions.add(new LatLng(startObj.getDouble("lat"), startObj.getDouble("lng")));
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                Polyline routeLines = mMap.addPolyline(lineOptions);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        mRequestQueue.add(stringRequest);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            com.example.android.multidex.forkyoupoop.PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            whereAmI = LocationServices.FusedLocationApi.getLastLocation(client);
            Log.d("CLIENT DATa?", String.format("%f asdf %f", whereAmI.getLatitude(), whereAmI.getLongitude()));

            if (whereAmI != null)
            {
                // Add a marker and move the camera
                LatLng currentLocation = new LatLng(whereAmI.getLatitude(), whereAmI.getLongitude());
                Marker currenPositionMarker = mMap.addMarker(new MarkerOptions().position(currentLocation).title("Current Location"));

                LatLng destination = new LatLng(placeLat, placeLng);
                Marker destinationMarker = mMap.addMarker(new MarkerOptions().position(destination).title(placeNamer));
                markersAndPlaceID.put(destinationMarker.getId(), placeIDr);

//                LatLngBounds center = new LatLngBounds(new LatLng(destination.latitude, destination.longitude), new LatLng(currentLocation.latitude, currentLocation.longitude));
                LatLng center = new LatLng((currentLocation.latitude+destination.latitude)/2, (currentLocation.longitude+destination.longitude)/2);
                //move camera in middle
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(center, 10));

                //call polylines function
                addPolylines(new LatLng(whereAmI.getLatitude(), whereAmI.getLongitude()), new LatLng(placeLat, placeLng));
                addPopularPlaces(center);
            }
        }
    }

    private void addPopularPlaces(LatLng centerPoint)
    {
        String baseURL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
        final String location = "location=" + centerPoint.latitude + "," + centerPoint.longitude;
        String radius = "radius=" + "10000";
        String serverKey = "key=" + "AIzaSyAuv0OXbFeQaly74HjYymnzZguvofv_FzY";
        String type = "type=" + "food";

        String fullURL = baseURL + location + "&" + radius + "&" + type + "&" + serverKey;
        Log.d("fullURL", fullURL);
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, fullURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Places", response);
                try
                {
                    JSONObject mainObj = new JSONObject(response);
                    JSONArray resultsArray = mainObj.getJSONArray("results");

                    for (int i = 0;i < resultsArray.length();i++)
                    {
                        JSONObject onePlace = resultsArray.getJSONObject(i);
                        JSONObject geometryObj = onePlace.getJSONObject("geometry");
                        JSONObject locationObj = geometryObj.getJSONObject("location");

                        Marker placesMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(locationObj.getDouble("lat"), locationObj.getDouble("lng")))
                            .title(onePlace.getString("name"))
                            .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));

                        markersAndPlaceID.put(placesMarker.getId(), onePlace.getString("place_id"));
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        mRequestQueue.add(stringRequest);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onMarkerClick(final Marker marker) {

        favouriteButton.setEventListener(new SparkEventListener(){
            @Override
            public void onEvent(ImageView button, boolean buttonState) {
                if (buttonState) {
                    // Button is active

                    //Firebase database save data
                    Firebase firebaseRef = new Firebase("https://Forkyoupoop.firebaseio.com/");
                    AuthData authData = firebaseRef.getAuth();
                    if (authData != null) {
                        // user authenticated
//                        Firebase usersRef = firebaseRef.child("users").child(authData.getUid());

                        Map<String, String> map = new HashMap<String, String>();
                        map.put("placeName", marker.getTitle());
                        map.put("placeID", markersAndPlaceID.get(marker.getId()));
                        map.put("placeLat", String.format("%f", marker.getPosition().latitude));
                        map.put("placeLng", String.format("%f", marker.getPosition().longitude));
                        map.put("userID", authData.getUid());
                        Firebase pushRef = firebaseRef.child("users").child(authData.getUid()).push();
                        pushRef.setValue(map);

                        String pushID = pushRef.getKey();
                        Map<String, Object> keyMap = new HashMap<String, Object>();
                        keyMap.put("pushID", pushID);
                        firebaseRef.child("users").child(authData.getUid()).child(pushID).updateChildren(keyMap, new Firebase.CompletionListener() {
                            @Override
                            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
//                                Log.d("Key updating", firebaseError.getDetails());
                                Toast.makeText(getApplicationContext(), "Saved to favourites", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        // no user authenticated. another bunch of code to add
                        Log.d("Firebase", "user not log in");
                    }

                }
                else
                {
                    // Button is inactive
                }
            }
        });

        return false;
    }

    @Override
    public void onInfoWindowClose(Marker marker) {
        favouriteButton.setChecked(false);
    }
}
