package com.example.android.multidex.forkyoupoop;

import android.content.Context;
import android.content.Intent;
import android.support.multidex.MultiDex;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.alpha.forkyoupoop.MainMenu;
import com.example.android.multidex.forkyoupoop.LoginActivity;
import com.example.android.multidex.forkyoupoop.PopularPlaces;
import com.example.android.multidex.forkyoupoop.RegisterActivity;
import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import java.util.Map;

public class UserRegistration extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_user_registration);

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add("Register", RegisterActivity.class)
                .add("Login", LoginActivity.class)
                .create());

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);
    }

    public void registerUser(View view) {
        EditText emailToRegister = (EditText)findViewById(R.id.userEmailAddressR);
        EditText passwordToRegister = (EditText)findViewById(R.id.userPasswordR);
        EditText passwordConfirm = (EditText)findViewById(R.id.userPasswordConfirmR);

        if (passwordToRegister.getText().toString().equals(passwordConfirm.getText().toString()))
        {
            Firebase ref = new Firebase("https://Forkyoupoop.firebaseio.com");
            ref.createUser(emailToRegister.getText().toString(), passwordToRegister.getText().toString(), new Firebase.ValueResultHandler<Map<String, Object>>() {
                @Override
                public void onSuccess(Map<String, Object> result) {
                    System.out.println("Successfully created user account with uid: " + result.get("uid"));
                    Toast.makeText(getApplicationContext(), "Success, proceed to login", Toast.LENGTH_LONG).show();
                }
                @Override
                public void onError(FirebaseError firebaseError) {
                    // there was an error
                    Log.d("firebase error", firebaseError.getDetails());
                }
            });
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Please make sure password match", Toast.LENGTH_LONG).show();
        }
    }

    public void loginUser(View view)
    {
        EditText emailLogin = (EditText)findViewById(R.id.userEmailAddress);
        EditText passwordLogin = (EditText)findViewById(R.id.userPassword);

        Firebase ref = new Firebase("https://Forkyoupoop.firebaseio.com");
        ref.authWithPassword(emailLogin.getText().toString(), passwordLogin.getText().toString(), new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                System.out.println("User ID: " + authData.getUid() + ", Provider: " + authData.getProvider());
//                Toast.makeText(getApplicationContext(), "Login Success", Toast.LENGTH_LONG).show();

                Intent goToMainMenu = new Intent(getApplicationContext(), MainMenu.class);
                startActivity(goToMainMenu);
            }
            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                // there was an error
                Toast.makeText(getApplicationContext(), "Email or Password Incorrect. Please try again", Toast.LENGTH_LONG).show();
            }
        });
    }
}
