package com.example.alpha.forkyoupoop;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.multidex.forkyoupoop.PopularPlaces;
import com.example.android.multidex.forkyoupoop.R;
import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.vision.text.Text;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.android.multidex.forkyoupoop.R.id.favouriteListView;

/**
 * Created by leikeze on 7/18/2016.
 */
public class CustomAdapter extends BaseAdapter{

    List<PlacesModel> favourites;
    Context context;
    private static LayoutInflater inflater=null;
    ListView theListView;
    TextView isThereData;

    public CustomAdapter(Activity mainActivity, List<PlacesModel> favourites, ListView theListView, TextView isThereData) {
        // TODO Auto-generated constructor stub
        this.favourites = favourites;
        this.isThereData = isThereData;
        context=mainActivity;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.theListView = theListView;
        //set firebase
        Firebase.setAndroidContext(context);
    }

    @Override
    public int getCount() {
        return favourites.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        TextView placeNameTextView;
        TextView latTextView;
        TextView lngTextView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.favourite_list, null);
        holder.placeNameTextView = (TextView)rowView.findViewById(R.id.placeNameTextView);
        holder.latTextView = (TextView)rowView.findViewById(R.id.latTextView);
        holder.lngTextView = (TextView)rowView.findViewById(R.id.lngTextView);

        holder.placeNameTextView.setText(favourites.get(position).getPlaceName());
        holder.latTextView.setText(String.format("Lat: %s", favourites.get(position).getPlaceLat()));
        holder.lngTextView.setText(String.format("Lng: %s", favourites.get(position).getPlaceLng()));

        Button deleteBtn = (Button)rowView.findViewById(R.id.delBtn);
        deleteBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
                //Firebase database delete data
                Firebase firebaseRef = new Firebase("https://Forkyoupoop.firebaseio.com/");
                final AuthData authData = firebaseRef.getAuth();
                if (authData != null) {
                    // user authenticated

                    firebaseRef.child("users").child(authData.getUid()).child(favourites.get(position).getPushID()).removeValue(new Firebase.CompletionListener() {
                        @Override
                        public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                            firebase = new Firebase("https://Forkyoupoop.firebaseio.com/users");
                            firebase.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot == null)
                                    {
                                        favourites.clear();
                                        CustomAdapter listViewAdapter = (CustomAdapter) theListView.getAdapter();
                                        listViewAdapter.notifyDataSetChanged();
                                        isThereData.setVisibility(View.VISIBLE);
                                    }
                                    else {
                                        Boolean userFound = false;
                                        for (DataSnapshot user : dataSnapshot.getChildren()) {

                                            if (user.getKey().equals(authData.getUid())) {
                                                userFound = true;
                                                favourites.clear();
                                                isThereData.setVisibility(View.INVISIBLE);

                                                for (DataSnapshot place : user.getChildren()) {
                                                    PlacesModel favourite = place.getValue(PlacesModel.class);
                                                    favourites.add(favourite);
                                                }
                                                CustomAdapter listViewAdapter = (CustomAdapter) theListView.getAdapter();
                                                listViewAdapter.notifyDataSetChanged();
                                            }
                                        }
                                        if (userFound == false) {
                                            favourites.clear();
                                            isThereData.setVisibility(View.VISIBLE);
                                            CustomAdapter listViewAdapter = (CustomAdapter) theListView.getAdapter();
                                            listViewAdapter.notifyDataSetChanged();
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(FirebaseError firebaseError) {

                                }
                            });
                        }
                    });


                } else {
                    // no user authenticated. another bunch of code to add
                    Log.d("Firebase", "user not log in to delete");
                }
            }
        });

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "You Clicked "+favourites.get(position).getPlaceName(), Toast.LENGTH_SHORT).show();

                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + favourites.get(position).getPlaceLat() + "," + favourites.get(position).getPlaceLng() );
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                context.startActivity(mapIntent);
            }
        });
        return rowView;
    }
}
